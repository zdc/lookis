package main

import (
	"flag"
	"fmt"
	"encoding/json"
	"log"
	"net"
	"net/http"
	"regexp"
	"sort"
	"github.com/spf13/viper"
	"golang.org/x/crypto/ssh"
)

// setting global variables
var AllConfigKeys []string
var datapath string
var RouterList []Router

// declare type for router commands
type RouterCommands struct {
	CommandId, CommandName string
}

// declare type for routers list
type Router struct {
	RouterId, RouterName string
	RouterCommands []RouterCommands
}

// declare variables for JSON interface
type JsonIn struct {
	Router, Action, Argument string
}
type JsonOut struct {
	Status, Result string
}

// fill response in JSON
func PrepareJson (StatusRaw, ResultRaw string) JsonOut {
	if StatusRaw == "Error" {
		log.Printf("Error! \"%s\"", ResultRaw)
	}
	output := JsonOut{ Status: StatusRaw, Result: ResultRaw }
	
	return output
}

// convert []byte slice to string
func ByteToString (byteinput []byte) string {
	byteslen := len(byteinput)
	stringoutput := string(byteinput[:byteslen])

	return stringoutput
}

// check for correct action and return command for it
func GetCommand (router, command string) (string, bool) {
	var AvalList []RouterCommands
	var Command string
	var CommandState bool

	AvalList = GetRouterCommands(router)
	for i := range AvalList {
		if AvalList[i].CommandId == command {
			Command = viper.GetString(router+".command_"+command)
			CommandState = true
		}
	}

	return Command, CommandState
}

// connect to router and run command
func SSHRun (router_ip, port, username, password, command, argument, suffix string) JsonOut {
	var rawoutput []byte
	var output JsonOut
	var allok bool = true

	// setting auth options
	sshConfig := &ssh.ClientConfig {
		User: username,
		Auth: []ssh.AuthMethod {
				ssh.Password(password),
		},
	}

	// connect to router
	connection, err := ssh.Dial("tcp", router_ip+":"+port, sshConfig)
	if err != nil {
		output = PrepareJson ("Error", "Unable to connect: "+router_ip)
		allok = false
	}

	if allok == true {
		// create ssh session
		session, err := connection.NewSession()
		if err != nil {
			output = PrepareJson ("Error", "Connection failed: failed to create session")
			allok = false
			connection.Close()
		} else {
			if allok == true {
				// run command and save output
				rawoutput, err = session.Output(command+" "+argument+suffix)
				if err != nil {
					output = PrepareJson ("Error", "Cannot execute command: "+command+" "+argument+suffix)
					allok = false
					connection.Close()
				} else {
					output = PrepareJson ("Result", ByteToString(rawoutput))
					connection.Close()
				}
			}
		}
	}

	return output
}

// get router name from config file
func GetRouterName (RouterId string) string {
	var RouterName string

	RouterName = viper.GetString(RouterId+".name")

	return RouterName
}

// check router in request
func CheckRouter (RouterId string) bool {
	var RouterStatus bool

	for i := range RouterList {
		if RouterList[i].RouterId == RouterId {
			RouterStatus = true
			break
		} else {
			RouterStatus = false
		}
	}

	return RouterStatus
}

// get command list for router from config file
func GetRouterCommands (RouterId string) []RouterCommands {
	var CommandList []RouterCommands
	
	for i := range AllConfigKeys {
		// find those, who match RouterId.command_XXX
		matchstring := RouterId+`.command_[(\d)]*$`
		matched, _ := regexp.MatchString(matchstring, AllConfigKeys[i])
		if matched == true {
			//get command name
			var strip = regexp.MustCompile(RouterId+".command_")
			var ListEntry RouterCommands
			id := strip.ReplaceAllString(AllConfigKeys[i], "")
			name := viper.GetString(RouterId+".command_"+id+"_name")
			ListEntry = RouterCommands{id, name}
			CommandList = append(CommandList, ListEntry)
		}
	}

	return CommandList
}

// parse enabled router list from config
func ParseRoutersFromConfig () []Router {
	var routers []Router
	var enabled string

	enabled = `router[\d]*.enabled`
	for i := range AllConfigKeys {
		// find those, who match routerNN.enabled
		matched, _ := regexp.MatchString(enabled, AllConfigKeys[i])
		if matched == true {
			//check if enabled = "yes"
			var strip = regexp.MustCompile(`\.enabled`)
			routerid := strip.ReplaceAllString(AllConfigKeys[i], "")
			if viper.GetString(routerid+".enabled") == "yes" {
				routername := GetRouterName(routerid)
				commandlist := GetRouterCommands(routerid)
				routers = append(routers, Router { RouterId: routerid, RouterName: routername, RouterCommands: commandlist })
			}
		}
	}
	
	return routers
}

// show JSON with routers and commands
func ShowJSON (w http.ResponseWriter, r *http.Request) {
	// check method
	switch r.Method {
	// if GET, then show router list with commands
	case "GET":
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		jsonout, _ := json.MarshalIndent(ParseRoutersFromConfig(), "", "\t")
		w.Write(jsonout)
	// if POST, process request and show answer
	case "POST":
		var Command, argument, suffix string
		var router_ip, port, username, password string
		var input_router, input_action, input_argument string
		var output JsonOut
		var routerState, CommandState bool
		var jsonrequest JsonIn

		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		// decode JSON to variable jsonrequest
		jsonbody := json.NewDecoder(r.Body)
		err := jsonbody.Decode(&jsonrequest)
		if err != nil {
			PrepareJson ("Error", "Cannot decode JSON")
			goto ShowOutput
		}
		
		input_router = jsonrequest.Router
		input_action = jsonrequest.Action
		input_argument = jsonrequest.Argument
		
		// check for selected router exist
		routerState = CheckRouter(input_router)
		if routerState == true {
				// setting router variables from config file
				router_ip = viper.GetString(input_router+".address")
				port = viper.GetString(input_router+".port")
				username = viper.GetString(input_router+".user")
				password = viper.GetString(input_router+".password")
		} else {
			output = PrepareJson ("Error", "Bad router id: "+input_router)
			goto ShowOutput
		}

		// check and get command for selected action
		Command, CommandState = GetCommand(input_router, input_action)
		if CommandState == false {
			output = PrepareJson ("Error", "Bad command: "+input_action)
			goto ShowOutput
		}

		// check for argument
		if net.ParseIP(input_argument) == nil {
			output = PrepareJson ("Error", "Bad argument: "+input_argument)
			goto ShowOutput
		} else {
			argument = input_argument
		}

		// connect to router and run command
		// check if command need suffix
		if viper.IsSet(input_router+".command_"+input_action+"_suffix") != false {
			suffix = " "+viper.GetString(input_router+".command_"+input_action+"_suffix")
		}
		output = SSHRun(router_ip, port, username, password, Command, argument, suffix)
		
		// show result to user
		ShowOutput:
		jsonout, _ := json.MarshalIndent(output, "", "\t")
		w.Write(jsonout)
	default:
		w.Write([]byte("Not allowed!"))
	}
}

func main() {
	// set config
	flag.StringVar(&datapath, "data", "", "data directory")
	flag.Parse()
	if datapath == "" {
		log.Fatal("Please, define \"-data=\" flag (directory with config and template files)")
	}
	viper.SetConfigName("config")
	viper.SetConfigType("toml")
	viper.AddConfigPath(".")
	viper.AddConfigPath(datapath)
	errconfig := viper.ReadInConfig() // Find and read the config file
	if errconfig != nil { // Handle errors reading the config file
		panic(fmt.Errorf("Fatal error config file: %s \n", errconfig))
	}

	// get all keys from config (used next in program)
	AllConfigKeys = viper.AllKeys()
	sort.Strings(AllConfigKeys)

	// read router list from config
	RouterList = ParseRoutersFromConfig()

	// set http server variables
	server_address := viper.GetString("server.address")
	server_port := viper.GetString("server.port")
	server_html := http.FileServer(http.Dir(datapath+"/html"))
	http.Handle("/", server_html)
	http.HandleFunc("/json", ShowJSON)

	// run server
	fmt.Println("lookis started")
	errserver := http.ListenAndServe(server_address+":"+server_port, nil)
	if errserver != nil {
		log.Fatal("ListenAndServe: ", errserver)
	}
}
