// setting global variables
var jsondata;

// get json data from server
$(function() {
	$.getJSON( "json", function (jsonraw) {
		jsondata = jsonraw;
		// fill actions for selected router
		FillInRouter();
		FillInAction();
	});
});

// action to do when router is selected
$(function() {
	$( "#router" ).change ( function () {
		// fill actions for selected router
		FillInAction();
	});
});

// handle "enter" key
$(function() {
	$('#argument').keypress( function (event) {
		var keycode = (event.keyCode ? event.keyCode : event.which);
		if (keycode == '13') {
			doit();
		}
	});
});

function addAction(OptionData, index, array) {
	var action = document.getElementById("action");
	var option = document.createElement("option");

	$.each(OptionData, function() {
	option.value = OptionData.CommandId;
		option.text = OptionData.CommandName;
		action.add(option);
	});
}

function FillInRouter() {
	var router = document.getElementById("router");

	// add routers to list
	$.each(jsondata, function(i, v) {
		var option = document.createElement("option");
		option.value = v.RouterId;
		option.text = v.RouterName;
		router.add(option);
	});
}

function FillInAction() {
	var router = document.getElementById("router");
	var RouterId = router.value;
	var action = document.getElementById("action");

	// clear action list
	$(action).empty()
	// add actions to list
	$.each(jsondata, function(i, v) {
		if (v.RouterId == RouterId) {
			v.RouterCommands.forEach(addAction);
		}
	});
}

function doit() {
	// get selected form values
	var router = document.getElementById("router").value;
	var action = document.getElementById("action").value;
	var argument = document.getElementById('argument').value;

	// format JSON
	var request_data = JSON.stringify({ "router": router, "action": action, "argument": argument });
	document.getElementById("status").innerHTML = "Please wait...";

	// make POST request to server with JSON data
	$.ajax({
	url: '/json',
	type: 'POST',
	contentType: 'application/json; charset=utf-8',
	data: request_data,
	dataType: "json",
	error: DrawTryLater,
	success: DrawResult 
	});
}

// slide up navigation bar
function SlideUpNav() {
	var navId = document.getElementById("navigation");
	var navTop = parseInt(window.getComputedStyle(navId).getPropertyValue("top"));
	document.getElementById("header").style.fontSize = "2.5em";

	for (i = 0; i < navTop; i++) {
		(function(i){
			var top = navTop - i + "px";
			window.setTimeout(function() { document.getElementById("navigation").style.top = top; }, i * 1);
		}(i));
	}
}

// show result to user
function DrawResult(jsonresult) {
	if (jsonresult['Status'] != "Error") {
		SlideUpNav();
	} else {
		document.getElementById("output-body").style.textAlign = "center";
		document.getElementById("output-body").style.marginLeft = "0";
	}
	document.getElementById("output-body").style.textAlign = "left";
	document.getElementById("output-body").style.marginLeft = "15%";
	document.getElementById("status").innerHTML = jsonresult['Status'];
	document.getElementById("output-body").innerHTML = jsonresult['Result'];
}

// ask user to try later
function DrawTryLater() {
	document.getElementById("output-body").style.textAlign = "center";
	document.getElementById("output-body").style.marginLeft = "0";
	document.getElementById("status").innerHTML = "Error";
	document.getElementById("output-body").innerHTML = "Request limit reached";
}
