### **lookis**: Looking Glass service on Go language
---
#### **About**
**lookis** is very simple and easy to configure service. All you need to start using it: configure access on routers, add items to configuration file and start lookis _(and build it, of course)_!
It can run configured command through SSH and show results in web browser.

---

#### **Building**
To build **lookis** you need:

* [**install Go language**](https://golang.org/doc/install) on your computer.
* install dependencies:
```
user@pc:/tmp$ go get "github.com/spf13/viper"
user@pc:/tmp$ go get "golang.org/x/crypto/ssh"
```
* clone **lookis** repository:
```
user@pc:/tmp$ git clone https://gitlab.com/zdc/lookis.git
```
* build executable file:
```
user@pc:/tmp$ cd lookis
user@pc:/tmp$ go build lookis.go
```

---

#### **Install**
After you build **lookis**:

* copy this files and directories to target system:
```
lookis
config.toml
html/
```
* place it to selected directory (**`/opt/lookis/`**, for example).
* edit **`config.toml`** to satisfy your needs.
	* in section **`[server]`** you can configure IP and port, where lookis has to run.
	* add/change sections **`[routerNN]`**, where **NN** - number from `"01"` and up (can be not in order).
	* there is a three example in **`config.toml`**, that explain what you can do with config.
* optionally: i reccomend to use **[nginx](https://nginx.org/)** for proxying requests to **lookis**. File **`extras/lookis.conf`** contain example how to do it.
* create **init.d** or **systemd-unit** file to start lookis automatically, when system load. You can use example in **`extras`** directory:
	* copy `extras/lookis.service` to `/etc/systemd/system/` on your server.
	* reload systemd manager configuration: 
`root@server:/tmp# systemctl daemon-reload`
	* enable **lookis** service:
`root@server:/tmp# systemctl enable lookis`
* check `/var/log/syslog`. You must see there `"lookis started"` if everything is OK.

Now all work was done. You can test your installation.

---

#### **Custom commands**
You can add custom commands for any router:
* open **`config.toml`** for edit.
* add next lines for needed section:
```
command_XX = "command to be executed"
command_XX_name = "command name to show in list"
command_XX_suffix = "suffix to command" (if needed)
```
where "XX" is number for this command in dropdown list.

---

#### **Сertain users / demo**
You can see **lookis** in work here:

* Argocom Ltd. (Ukraine): [**http://lg.acom.net.ua/**](http://lg.acom.net.ua/)
